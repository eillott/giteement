# giteement

#### 项目介绍
基于码云的评论系统，主要用于Hexo博客系统中接入基于码云Issues的评论系统。
画面UI近期计划优化，暂时效果如下图：  
![@功能](https://giteement.oss-cn-beijing.aliyuncs.com/%40%E5%9B%9E%E5%A4%8D%E5%8A%9F%E8%83%BD.png)
#### 软件架构
软件是基于[gitment](https://github.com/imsun/gitment)的源码开发的。  
开发语言：nodeJS

#### 使用说明
[文档](https://wudong.tech/2018/08/29/Hexo%E6%8E%A5%E5%85%A5%E7%A0%81%E4%BA%91%E8%AF%84%E8%AE%BA%E7%B3%BB%E7%BB%9F/)

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
