const path = require('path')

module.exports = {
    context: path.join(__dirname, 'src'),
    entry: './oauthcallback.js',
    devtool: 'source-map',
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'oauthcallback.browser.js',
        libraryTarget: 'var',
        library: 'Oauthcallback',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            "env"
                        ]
                    }
                },
                exclude: /^node_mocules/,
            },
        ],
    },
    devServer: {
        contentBase: "./test",//本地服务器所加载的页面所在的目录
        historyApiFallback: true,//不跳转
        inline: true//实时刷新
    }
}