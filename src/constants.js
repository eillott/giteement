export const LS_ACCESS_TOKEN_KEY = 'giteement-comments-token'
export const LS_USER_KEY = 'giteement-user-info'

export const NOT_INITIALIZED_ERROR = new Error('Comments Not Initialized')