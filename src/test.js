var express = require("express");
var https = require("https");
var app = express();
var hostName = '127.0.0.1';
var port = 8080;

app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By",' 3.2.1')
    res.header("Content-Type", "application/json;charset=utf-8");
    next();
});

app.get("/get",function(req,res){
    console.log("请求url：",req.path)
    console.log("请求参数：",req.query)
    res.send("这是get请求");
})
app.get("/taken", function(req, res){
    let code = req.query.code;
    let headers = req.headers;
    let path = "/oauth/token";
    let hostname = 'gitee.com';
    path += '?client_id=' + '11f02c3f93a28488215ae3c3fda47a5b94d8f60aea0fc1640ca88b353a150f6d';
    path += '&client_secret='+ '2786043fc3856082703cb7f8d63066aabecf5673b6cb4460d7343b860a8ddfd1';
    path += '&code='+ code;
    path += '&grant_type='+ 'authorization_code';
    console.log(path);
    let opts = {
        hostname: hostname,
        port:'443',
        path:path,
        headers:headers,
        method:'POST'
    };

    req = https.request(opts, function(res){
        alert(res);
    })
})
app.listen(port,hostName,function(){

    console.log(`服务器运行在http://${hostName}:${port}`);

});